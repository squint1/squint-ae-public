# Squint Imaging Evaluation 
This repository contains the artifacts of our MobiCom 2023 paper titled “Squint: A framework for Dynamic Voltage Scaling of Image Sensors Towards Low Power IoT Vision”. If you would like to cite this work, you can use the following Zenodo DOI:

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8270962.svg)](https://doi.org/10.5281/zenodo.8270962)

The evaluation consists of a dataset of images, recorded power traces, and the collection of benchmark evaluation and graphing scripts. 

**Goals** 
1. Reproduce the benchmark results in the paper 
2. Showcase the analysis methods behind the paper conclusions 

# Setup
## Artifact Sources 
This artifact is available in two formats: a docker container and a repository hosted on GitLab.

For both formats, download code and data.
### Code
Clone the repository. 

`git clone https://gitlab.com/squint1/squint-ae-public.git` 

### Data
Download and unzip the dataset (est. 3 - 5 hrs, depending on internet speed). 
Note this is a large download (30+ GB). You can choose to do this later, if you want to use pre-recorded data for result reproduction as a starting point.

```
curl -L -o data.zip "https://zenodo.org/record/8172535/files/squint-dataset.zip?download=1"
unzip data.zip -d data
```
After these steps, your repository will have the following structure. 

```
├── data/
├── code/
│   ├── camera_pose_estimation
│   ├── data_analysis /
│   │   └── figures /
│   │       └── AEfigures.ipynb
│   ├── mAP
│   └── yolo
├── req.txt
└── Dockerfile
```

## Docker (est. 3-5 min)
This requires [Docker](https://www.docker.com/). To get started, simply run

```
docker run -it -p 8888:8888 -v $PWD/data:/squint/data kodukulav/squint_ae_eval:latest
```

If you prefer to build the docker image locally:

```
docker build -t squint_ae_eval . 
docker run -it -p 8888:8888 -v $PWD/data:/squint/data squint_ae_eval:latest
```

You will now have a terminal open in the container.

## Local Machine (10-20 Minutes) 

Running locally requires a python3 install along with pip3. Tested and verified with python 3.8, but other comparable versions should also work. 

### Install dependencies 
Install the required python packages using:
`python3 -m pip install -r req.txt` 

## Launch notebook 
`jupyter notebook --ip=0.0.0.0 --port=8888 --no-browser --allow-root` 

This step will create a URL that can be opened in a browser on your host machine. An example URL is shown below.
`http://127.0.0.1:8888/?token=a5e5d88bcfcd2c56e1dc9f439a898eb8b34f69e4f5e9aed0`

Once inside Jupyter Notebook on the browser, open the following notebook
`/code/data_analysis/figures/AEfigures.ipynb` 

Congrats! You should now be ready to run the artifact.

# Step-by-Step Guide

- Pre-recorded flow: Generate the charts present in the paper using the traces collected from the system
    - Motivation figures (Figs. 3, 4, 5c, 6)
    - Voltage traces (Figs. 13 and 14)    
    - Benchmark figures (Figs. 15 and 16)
    - Power trace (Fig 17)
- Dataset based flow:
Generate benchmark accuracy figures (Figs. 15a and 16a) using experimental dataset
    - Single-policy: Collect benchmark data for a given policy
    - Multi-policy: Collect benchmark for multiple policies

# Pre-recorded flow (5 min)

To enable this, set all the calculation flags to "False" in the notebook. 

* `Calculate_Person_Detection = False`
* `Detection_Sq_OD = False`
* `Detection_Sq_Rand = False`
* `Detection_Sq_Light = False`
* `Detection_RR = False`

Page through the notebook step-by-step to generate figures
### Calculate Power Measurements (2-3 Minutes)
Calculates power measurements from recorded voltage trace csv files. Outputs tables of the average power use vs voltage and sensor mode.

### Figure 3 (<30 seconds)
Generates Power vs Voltage plot 

### Figure 5c (<30 seconds) 
Image histogram 

### Figs. 13 and 14 (<30 seconds) 
Voltage traces 

### Figure 15b (<30 seconds) 
Power vs Policy for People detection workload

### mAP Calculations 
Here is where the person detection and mAP values are calculated, if specified in the flags. This process is the most time-consuming part of the evaluation. Each policy, represented by a flag such as Detection_Sq_Rand = True, takes approximately 1 hour or more for person detection using YOLO, and an additional 15 minutes to calculate mAP. The total time required per policy evaluation is approximately 1 hour and 15 minutes. Please note that hardware configuration significantly affects this process, especially GPU acceleration availability.

### Figure 15a (<30 seconds)
mAP vs policy for People detection workload

### Figs. 16a & 16b (5 minutes)
Accuracy and Power chart for Camera Pose Estimation workload

### Figure 4 (<30 seconds) 
Plots Dark signal vs. voltage

### Figure 6 (<30 seconds) 
mAP vs Voltages 

### Figure 17 (<30 seconds) 
Power traces

# Dataset based flow (3-12 hours)
To enable this, set the main calculation flag to "True" first.

* `Calculate_Person_Detection = True`

## Single-policy (~3 hrs)

Set the policy specific flag that you'd like to execute. An example flag configuration for "Random" policy is shown below.

* `Detection_Sq_OD = False`
* `Detection_Sq_Rand = True`
* `Detection_Sq_Light = False`
* `Detection_RR = False`

In the notebook menu, Cell->Run All. This would generate Figs. 15a and 16a by running the benchmarks, i.e., People Detection and Camera Pose Estimation, on the dataset for the specified policy. Other policy data is generated from pre-recorded traces.

## Multi-policy (10-12 hrs)

Set all the policy flags to "True"

* `Detection_Sq_OD = True`
* `Detection_Sq_Rand = True`
* `Detection_Sq_Light = True`
* `Detection_RR = True`

In the notebook menu, Cell->Run All. This would generate Figs. 15a and 16a using the dataset evaluation results for all the policies listed in the paper.
