import sys
import numpy as np
import matplotlib.pyplot as plt

def full_plot(path):
    plt.figure(figsize=(9,2))
    data = np.genfromtxt(path, skip_header=1, delimiter=',', names=True)
    cols = iter(data.dtype.names)
    for s, v in zip(cols, cols):
        plt.plot(data[s], data[v], '.-', label='ch %s' % [int(d) for d in s if d.isdigit()][0], linewidth=0.5, markersize=1)
    plt.tight_layout()
    plt.legend(loc='upper right')
    #plt.savefig('plot.png', dpi=100)  
    plt.show()