import colorsys
import os
from pathlib import Path
import cv2


import numpy as np
from image_pose_estimation import pose_esitmation
from utils import ARUCO_DICT
import pandas as pd



class Pose_Detect: 
    
    def __init__(self,data_path,voltages,distances,RR): 
        #initialize 
        self.RR = RR
        self.voltages = voltages
        if RR: 
            self.voltages.append("RR")
        self.distances = distances
        self.CROP_E = (190,940)
        self.CROP_GT = (235,985)
        self.data_path = data_path


    def detect(self):
        rotation_results = {}
        translation_results = {}
        voltages=self.voltages
        distances = self.distances
        RR = self.RR
        CROP_E = self.CROP_E
        CROP_GT = self.CROP_GT
        #load distortion files 
        k = np.load("../../camera_pose_estimation/ArUCo-Markers-Pose-Estimation-Generation-Python//calibration_matrix.npy")
        d = np.load("../../camera_pose_estimation/ArUCo-Markers-Pose-Estimation-Generation-Python//distortion_coefficients.npy")
        aruco_dict_type = ARUCO_DICT["DICT_7X7_250"]
        #voltages = ['']
        for dist in distances:
            #across voltages 
            #norm variables 
            tvec_norm_list = []
            rvec_norm_list = []
            for volt in voltages: 
                if(volt == "RR"):
                    volt =2.8
                    RR = True
                MAIN_PATH = f"{self.data_path}/{dist}/{volt}/"# f"../data/policies/random/{volt}/"
                #norm variables 
                tvec_norm = []
                rvec_norm = []




                files = os.listdir(MAIN_PATH)
                files_base = list(filter(lambda x : (x[0]=='g') and (x[-1]=='g'),files))
                files_base = list(map(lambda x: x[2:],files_base))

                for idx,file_img in enumerate(files_base): 
                    file_gt = "gt"+file_img
                    file_e  = "e" +file_img 
                    #load images 
                    #print(MAIN_PATH+file_gt)

                    image_gt = cv2.imread(MAIN_PATH+file_gt, cv2.IMREAD_COLOR)
                    image_e  = cv2.imread(MAIN_PATH+file_e , cv2.IMREAD_COLOR)
                    #crop images 
                    image_gt = image_gt[:,CROP_GT[0]:CROP_GT[1],:]
                    image_e = image_e[:,CROP_E[0]:CROP_E[1],:]
                    if(RR):
                        image_e = cv2.resize(image_e, (640,480)) 


                    #classify images
                    r_image_gt, rvec_gt,tvec_gt = pose_esitmation(image_gt, aruco_dict_type, k, d)
                    r_image_e, rvec_e, tvec_e = pose_esitmation(image_e, aruco_dict_type, k, d)
                    #print(rvec_gt,tvec_gt)
                    #print(rvec_e, tvec_e)

                    #calculate norms 
                    tvec_norm.append(np.abs((np.linalg.norm(np.abs(tvec_gt) - np.abs(tvec_e)))))
                    rvec_norm.append(np.abs((np.linalg.norm(np.abs(rvec_gt) - np.abs(rvec_e)))))



                tvec_norm_list.append(tvec_norm)
                rvec_norm_list.append(rvec_norm)
                #print(volt,file_img,tvec_norm)

            t_df = pd.DataFrame(np.transpose(np.array(tvec_norm_list)),columns=voltages)
            r_df = pd.DataFrame(np.transpose(np.array(rvec_norm_list)),columns=voltages)
            

            
                   
            rotation_results[dist] = dict(list(zip(r_df.keys(), list(zip(r_df.mean(),r_df.std()**2)))))

            translation_results[dist] = dict(list(zip(t_df.keys(), list(zip(t_df.mean(),t_df.std()**2)))))
                
            
        return({"rotation_results":rotation_results, "translation_results":translation_results})
            
            







