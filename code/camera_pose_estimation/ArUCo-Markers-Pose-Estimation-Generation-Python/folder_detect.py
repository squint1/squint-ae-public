import colorsys
import os
from pathlib import Path
import cv2


import numpy as np
from image_pose_estimation import pose_esitmation
from utils import ARUCO_DICT
import pandas as pd



#initialize 
RR = True
voltages = ["2.8","2.0","1.8","1.4"]
distances = ["close","far","mid"]
SAVE_IMG = True
IMG_PATH = "input/images-optional/"
COMBINE_IMG = False 
IMG_PATH_COMB = "input/images-combine/" # "input/images-combine-person/"
CSV_PATH_OUT = "../../data_analysis/pose_estimation/data/"
CROP_E = (190,940)
CROP_GT = (235,985)

#load distortion files 
k = np.load("./calibration_matrix.npy")
d = np.load("./distortion_coefficients.npy")
aruco_dict_type = ARUCO_DICT["DICT_7X7_250"]


#voltages = ['']
for dist in distances:
    #across voltages 
    #norm variables 
    tvec_norm_list = []
    rvec_norm_list = []
    for volt in voltages: 
        MAIN_PATH = f"../../data/pose/{dist}/{volt}/"# f"../data/policies/random/{volt}/"
        #norm variables 
        tvec_norm = []
        rvec_norm = []



        #create paths if non-existant 
        Path(MAIN_PATH+IMG_PATH).mkdir(parents=True, exist_ok=True)
        Path(MAIN_PATH+IMG_PATH_COMB).mkdir(parents=True, exist_ok=True)



        files = os.listdir(MAIN_PATH)
        files_base = list(filter(lambda x : (x[0]=='g') and (x[-1]=='g'),files))
        files_base = list(map(lambda x: x[2:],files_base))

        for idx,file_img in enumerate(files_base): 
            file_gt = "gt"+file_img
            file_e  = "e" +file_img 
            #load images 
            #print(MAIN_PATH+file_gt)

            image_gt = cv2.imread(MAIN_PATH+file_gt, cv2.IMREAD_COLOR)
            image_e  = cv2.imread(MAIN_PATH+file_e , cv2.IMREAD_COLOR)
            #crop images 
            image_gt = image_gt[:,CROP_GT[0]:CROP_GT[1],:]
            image_e = image_e[:,CROP_E[0]:CROP_E[1],:]
            if(RR):
                image_e = cv2.resize(image_e, (640,480)) 

            #save image 
            if(SAVE_IMG):
                cv2.imwrite(MAIN_PATH+IMG_PATH+file_img[:-4]+'.jpg',image_gt)

            #classify images
            r_image_gt, rvec_gt,tvec_gt = pose_esitmation(image_gt, aruco_dict_type, k, d)
            r_image_e, rvec_e, tvec_e = pose_esitmation(image_e, aruco_dict_type, k, d)
            #print(rvec_gt,tvec_gt)
            #print(rvec_e, tvec_e)

            #calculate norms 
            tvec_norm.append(np.abs((np.linalg.norm(np.abs(tvec_gt) - np.abs(tvec_e)))))
            rvec_norm.append(np.abs((np.linalg.norm(np.abs(rvec_gt) - np.abs(rvec_e)))))


            if(COMBINE_IMG):
                #combine images
                comb_img = np.concatenate((r_image_gt,r_image_e),axis=1)
                #save combined image 
                cv2.imwrite(MAIN_PATH+IMG_PATH_COMB+file_img[:-4]+'.jpg',comb_img)



            if(idx % 100 == 0):
                print(idx/len(files_base))

        tvec_norm_list.append(tvec_norm)
        rvec_norm_list.append(rvec_norm)
        print(volt,file_img,tvec_norm)

    t_df = pd.DataFrame(np.transpose(np.array(tvec_norm_list)),columns=voltages)
    r_df = pd.DataFrame(np.transpose(np.array(rvec_norm_list)),columns=voltages)
    if(RR):
        t_df.to_csv(CSV_PATH_OUT+f"{dist}_tvec_RR.csv")
        r_df.to_csv(CSV_PATH_OUT+f"{dist}_rvec_RR.csv")
    else: 
        t_df.to_csv(CSV_PATH_OUT+f"{dist}_tvec.csv")
        r_df.to_csv(CSV_PATH_OUT+f"{dist}_rvec.csv")
 






