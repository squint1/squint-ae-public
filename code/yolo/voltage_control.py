import requests
import json

class V_Ctrl: 
    def __init__(self):
        self.ip_e = "10.208.17.232"
        self.ip_gt = "10.208.20.33"
        self.voltage_times = {"series":[],
        "2.8":0,
        "2.7":0,
        "2.6":0,
        "2.5":0,
        "2.4":0,
        "2.3":0,
        "2.2":0,
        "2.1":0,
        "2.0":0,
        "1.9":0,
        "1.8":0,
        "1.7":0,
        "1.6":0,
        "1.5":0,
        "1.4":0
        }

    def set_voltage(self,voltage):
        voltage = round(voltage,1)
        set_voltage_endpoint = f"http://{self.ip_e}:5050/set?voltage="
        set_voltage_get = requests.get(set_voltage_endpoint + str(voltage))
        self.voltage_times[str(voltage)] = self.voltage_times[str(voltage)] + 1
        self.voltage_times["series"].append(voltage)

    def write_times(self,path):
        json_object = json.dumps(self.voltage_times, indent = 4)
        with open(path,"w") as f:
            f.write(json_object)
    
    def measure(self,period,name):
        start_measure_endpoint = f"http://{self.ip_e}:5050/measure/cont/time?T={period}&name={name}"
        start_measure_get = requests.get(start_measure_endpoint)

    def set_exposure(self,exposure):
        for ip in [self.ip_e, self.ip_gt] :
            start_measure_endpoint = f"http://{ip}:5050/set/exposure?exposure={exposure}"
            start_measure_get = requests.get(start_measure_endpoint)
        

