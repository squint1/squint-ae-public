import colorsys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
import cv2
import time
import queue
import threading
import multiprocessing as mp
import vid_streamv3 as vs
import sys
from pathlib import Path
import requests



q = queue.Queue()

set_voltage_endpoint = "http://10.153.94.217:5050/set?voltage="
import numpy as np
import tensorflow.compat.v1.keras.backend as K
import tensorflow as tf
tf.compat.v1.disable_eager_execution()
#from keras import backend as K
from keras.models import load_model
from keras.layers import Input

from yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3.utils import image_preporcess

LABELS = []

class YOLO(object):
    _defaults = {
        #"model_path": 'logs/trained_weights_final.h5',
        "model_path": 'model_data/yolo_weights.h5',
        "anchors_path": 'model_data/yolo_anchors.txt',
        "classes_path": 'model_data/coco_classes.txt',
        "score" : 0.3,
        "iou" : 0.45,
        "model_image_size" : (416, 416),
        "text_size" : 3,
    }

    @classmethod
    def get_defaults(cls, n):
        if n in cls._defaults:
            return cls._defaults[n]
        else:
            return "Unrecognized attribute name '" + n + "'"

    def __init__(self, **kwargs):
        self.__dict__.update(self._defaults) # set up default values
        self.__dict__.update(kwargs) # and update with user overrides
        self.class_names = self._get_class()
        self.anchors = self._get_anchors()
        self.sess = K.get_session()
        self.boxes, self.scores, self.classes = self.generate()

    def _get_class(self):
        classes_path = os.path.expanduser(self.classes_path)
        with open(classes_path) as f:
            class_names = f.readlines()
        class_names = [c.strip() for c in class_names]
        return class_names

    def _get_anchors(self):
        anchors_path = os.path.expanduser(self.anchors_path)
        with open(anchors_path) as f:
            anchors = f.readline()
        anchors = [float(x) for x in anchors.split(',')]
        return np.array(anchors).reshape(-1, 2)

    def generate(self):
        model_path = os.path.expanduser(self.model_path)
        assert model_path.endswith('.h5'), 'Keras model or weights must be a .h5 file.'

        # Load model, or construct model and load weights.
        num_anchors = len(self.anchors)
        num_classes = len(self.class_names)
        is_tiny_version = num_anchors==6 # default setting
        try:
            self.yolo_model = load_model(model_path, compile=False)
        except:
            self.yolo_model = tiny_yolo_body(Input(shape=(None,None,3)), num_anchors//2, num_classes) \
                if is_tiny_version else yolo_body(Input(shape=(None,None,3)), num_anchors//3, num_classes)
            self.yolo_model.load_weights(self.model_path) # make sure model, anchors and classes match
        else:
            assert self.yolo_model.layers[-1].output_shape[-1] == \
                num_anchors/len(self.yolo_model.output) * (num_classes + 5), \
                'Mismatch between model and given anchor and class sizes'

        print('{} model, anchors, and classes loaded.'.format(model_path))

        # Generate colors for drawing bounding boxes.
        hsv_tuples = [(x / len(self.class_names), 1., 1.)
                      for x in range(len(self.class_names))]
        self.colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
        self.colors = list(
            map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)),
                self.colors))

        np.random.shuffle(self.colors)  # Shuffle colors to decorrelate adjacent classes.

        # Generate output tensor targets for filtered bounding boxes.
        self.input_image_shape = K.placeholder(shape=(2, ))
        boxes, scores, classes = yolo_eval(self.yolo_model.output, self.anchors,
                len(self.class_names), self.input_image_shape,
                score_threshold=self.score, iou_threshold=self.iou)
        return boxes, scores, classes

    def detect_image(self, image,labels):
        if self.model_image_size != (None, None):
            assert self.model_image_size[0]%32 == 0, 'Multiples of 32 required'
            assert self.model_image_size[1]%32 == 0, 'Multiples of 32 required'
            boxed_image = image_preporcess(np.copy(image), tuple(reversed(self.model_image_size)))
            image_data = boxed_image

        out_boxes, out_scores, out_classes = self.sess.run(
            [self.boxes, self.scores, self.classes],
            feed_dict={
                self.yolo_model.input: image_data,
                self.input_image_shape: [image.shape[0], image.shape[1]],#[image.size[1], image.size[0]],
                K.learning_phase(): 0
            })

        #print('Found {} boxes for {}'.format(len(out_boxes), 'img'))

        thickness = (image.shape[0] + image.shape[1]) // 600
        fontScale=1
        ObjectsList = []

        for i, c in reversed(list(enumerate(out_classes))):
            predicted_class = self.class_names[c]
            box = out_boxes[i]
            score = out_scores[i]

            #label = '{} {:.2f}'.format(predicted_class, score)
            label = '{}'.format(predicted_class)
            scores = '{:.2f}'.format(score)

            top, left, bottom, right = box
            top = max(0, np.floor(top + 0.5).astype('int32'))
            left = max(0, np.floor(left + 0.5).astype('int32'))
            bottom = min(image.shape[0], np.floor(bottom + 0.5).astype('int32'))
            right = min(image.shape[1], np.floor(right + 0.5).astype('int32'))

            mid_h = (bottom-top)/2+top
            mid_v = (right-left)/2+left
                        
            if(labels == None or predicted_class in labels): #only works due to python being lazy. None is not iteratble 
                # put object rectangle
                cv2.rectangle(image, (left, top), (right, bottom), self.colors[c], thickness)

                # get text size
                (test_width, text_height), baseline = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, thickness/self.text_size, 1)

                # put text rectangle
                cv2.rectangle(image, (left, top), (left + test_width, top - text_height - baseline), self.colors[c], thickness=cv2.FILLED)

                # put text above rectangle
                cv2.putText(image, label, (left, top-2), cv2.FONT_HERSHEY_SIMPLEX, thickness/self.text_size, (0, 0, 0), 1)

                ObjectsList.append([top, left, bottom, right, mid_v, mid_h, label, scores])

        return image, ObjectsList

    def close_session(self):
        self.sess.close()

    def detect_img(self, image,labels=None):
        #image = cv2.imread(image, cv2.IMREAD_COLOR)
        original_image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        original_image_color = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)

        r_image, ObjectsList = self.detect_image(original_image_color,labels)
        return r_image, ObjectsList


def Receive():
    print('start receive')
    cap = cv2.VideoCapture('rtsp://10.208.23.206:8554/unicast', cv2.CAP_FFMPEG)
    ret, frame = cap.read()
    q.put(frame)
    while ret:
        ret, frame = cap.read()
        q.put(frame)
    cap.release()

def Process():
    yolo = YOLO()
    print("start processing")

    # set start time to current time
    start_time = time.time()
    # displays the frame rate every 2 second
    display_time = 2
    # Set primarry FPS to 0
    fps = 0

    while True:
        if q.empty() != True:
            frame = q.get()
            # resize our captured frame if we need
            frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

            # detect object on our frame
            r_image, ObjectsList = yolo.detect_img(frame)

            # show us frame with detection
            cv2.imshow("Web cam input", r_image)
            if cv2.waitKey(25) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break

            # calculate FPS
            fps += 1
            TIME = time.time() - start_time
            if TIME > display_time:
                print("FPS:", fps / TIME)
                fps = 0
                start_time = time.time()

    cv2.destroyAllWindows()
    yolo.close_session()


'''
Main class
Source: https://github.com/sahilparekh/GStreamer-Python/blob/master/main_prg.py
'''
class mainStreamClass:
    def __init__(self):

        #Current Cam
        self.camProcess = None
        self.cam_queue = None
        self.stopbit = None
        self.camlink = 'rtsp://10.208.17.232:8554/unicast' #Add your RTSP cam link
        self.framerate = 30

    def startMain(self):

        #Initialize YOLO
        yolo = YOLO()

        #set  queue size
        self.cam_queue = mp.Queue(maxsize=100)

        #get all cams
        time.sleep(3)

        self.stopbit = mp.Event()
        self.camProcess = vs.StreamCapture(self.camlink,
                             self.stopbit,
                             self.cam_queue,
                            self.framerate)
        self.camProcess.start()

        # calculate FPS
        lastFTime = time.time()

        # frame counter
        cnt = 0

        try:
            while True:

                if not self.cam_queue.empty():
                    # print('Got frame')
                    cmd, frame = self.cam_queue.get()


                    #calculate FPS
                    diffTime = time.time() - lastFTime
                    fps = 1 / diffTime
                    print(fps)

                    lastFTime = time.time()

                    # if cmd == vs.StreamCommands.RESOLUTION:
                    #     pass #print(val)

                    if cmd == vs.StreamCommands.FRAME:
                        if frame is not None:
                            # Configure camera voltage
                            set_succ = False
                            # Random policy: Reconfigure voltage every other frame
                            if cnt%2 == 0:
                                voltage = 2.8
                            else:
                                voltage = 2.8

                            # while(not set_succ):
                            #     #set voltage via end point
                            #     set_voltage_get = requests.get(set_voltage_endpoint + str(voltage))
                            #     print(set_voltage_get.json())
                            #     if(set_voltage_get.json()["success"] == True):
                            #         set_succ = True
                            #         print("GET SUCESS")
                            #     else:
                            #         time.sleep(15)

                            # Increment frame counter
                            cnt = cnt + 1


                            # resize our captured frame if we need
                            frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

                            # detect object on our frame
                            r_image, ObjectsList = yolo.detect_img(frame)
                            print(ObjectsList)

                            # show us frame with detection
                            cv2.imshow('Cam: ' + self.camlink, r_image)
                            cv2.waitKey(1)

        except KeyboardInterrupt:
            print('Caught Keyboard interrupt')

        except:
            e = sys.exc_info()
            print('Caught Main Exception')
            print(e)

        self.stopCamStream()
        cv2.destroyAllWindows()
        yolo.close_session()


    def stopCamStream(self):
        print('in stopCamStream')

        if self.stopbit is not None:
            self.stopbit.set()
            while not self.cam_queue.empty():
                try:
                    _ = self.cam_queue.get()
                except:
                    break
                self.cam_queue.close()

            self.camProcess.join()



if __name__=="__main__":
    # yolo = YOLO()

    # p1 = threading.Thread(target = Receive)
    # p2 = threading.Thread(target = Process)
    # p1.start()
    # p2.start()

    # set start time to current time
    # start_time = time.time()
    # # displays the frame rate every 2 second
    # display_time = 2
    # # Set primarry FPS to 0
    # fps = 0

    # we create the video capture object cap
    # cap = cv2.VideoCapture(0)
    # if not cap.isOpened():
    #    raise IOError("We cannot open webcam")

    # RTSP_URL = 'rtsp://10.208.17.232:8554/unicast'

    # cap = cv2.VideoCapture(RTSP_URL, cv2.CAP_FFMPEG)
    # if not cap.isOpened():
    #    print('Cannot open RTSP stream')
    #    exit(-1)

    #Labels 
    LABELS = ["person"]

    mc = mainStreamClass()
    mc.startMain()

    # while True:
    #     ret, frame = cap.read()
    #     if not(ret):
    #         print('No frame')
    #         exit(-1)
    #     # resize our captured frame if we need
    #     frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

    #     # detect object on our frame
    #     r_image, ObjectsList = yolo.detect_img(frame)

    #     # show us frame with detection
    #     cv2.imshow("Web cam input", r_image)
    #     if cv2.waitKey(25) & 0xFF == ord("q"):
    #         cv2.destroyAllWindows()
    #         break

    #     # calculate FPS
    #     fps += 1
    #     TIME = time.time() - start_time
    #     if TIME > display_time:
    #         print("FPS:", fps / TIME)
    #         fps = 0
    #         start_time = time.time()


    # cap.release()
    # cv2.destroyAllWindows()
    # yolo.close_session()
