
'''\
This Simple program Demonstrates how to use G-Streamer and capture RTSP Frames in Opencv using Python
- Sahil Parekh
'''

import multiprocessing as mp
import time
import vid_streamv3 as vs
import cv2
import sys
import numpy as np

'''
Main class
'''
class Record:
    def __init__(self,framerate,time,path):

        #Current Cam
        self.camProcess = None
        self.cam_queue = None
        self.stopbit = None
        self.camlink = 'rtsp://10.208.17.232:8554/unicast' #Add your RTSP cam link
        self.camlink_gt = 'rtsp://10.208.20.33:8554/unicast'

        self.framerate = framerate
        self.time_limit = time
        self.path = path
    
    def startMain(self):

        #set  queue size
        self.cam_queue = mp.Queue(maxsize=5)
        self.cam_queue_gt = mp.Queue(maxsize=5)

        #get all cams
        time.sleep(3)

        self.stopbit = mp.Event()
        self.camProcess = vs.StreamCapture(self.camlink,
                             self.stopbit,
                             self.cam_queue,
                            self.framerate)

        self.camProcess_gt = vs.StreamCapture(self.camlink_gt,
                        self.stopbit,
                        self.cam_queue_gt,
                    self.framerate)
        
        self.camProcess.start()
        self.camProcess_gt.start()

        # calculate FPS
        lastFTime = time.time()
        start_time = time.time()

        frame_num = 0
        try:
            while (time.time() - start_time) < self.time_limit:

                if (not self.cam_queue.empty()) and (not self.cam_queue_gt.empty()):
                    # print('Got frame')
                    cmd, val = self.cam_queue.get()
                    cmd_gt, val_gt = self.cam_queue_gt.get()
                    '''
                    #calculate FPS
                    diffTime = time.time() - lastFTime`
                    fps = 1 / diffTime
                    # print(fps)
                    
                    '''
                    lastFTime = time.time()

                    # if cmd == vs.StreamCommands.RESOLUTION:
                    #     pass #print(val)

                    if cmd == vs.StreamCommands.FRAME:
                        if val is not None:
                            #cv2.imshow('Cam: ' + self.camlink, val)
                            #cv2.imshow('Cam: ' + self.camlink_gt, val_gt)
                            cv2.imwrite(f"{self.path}/e_N.A_{frame_num}.jpg",val)
                            cv2.imwrite(f"{self.path}/gt_N.A_{frame_num}.jpg",val_gt)
                            #comb_img = np.concatenate((val,val_gt),axis=1)
                            #cv2.imwrite(f"{self.path}/c_N.A_{frame_num}.jpg",comb_img)
                            cv2.waitKey(1)

                    frame_num = frame_num + 1

        except KeyboardInterrupt:
            print('Caught Keyboard interrupt')

        except:
            e = sys.exc_info()
            print('Caught Main Exception')
            print(e)

        self.stopCamStream()
        cv2.destroyAllWindows()


    def stopCamStream(self):
        print('in stopCamStream')

        if self.stopbit is not None:
            self.stopbit.set()
            while not self.cam_queue.empty():
                try:
                    _ = self.cam_queue.get()
                except:
                    break
                self.cam_queue.close()

            self.camProcess.join()

        if self.stopbit is not None:
            self.stopbit.set()
            while not self.cam_queue_gt.empty():
                try:
                    _ = self.cam_queue_gt.get()
                except:
                    break
                self.cam_queue_gt.close()

            self.camProcess_gt.join()


if __name__ == "__main__":
    #sys arguments 
    # record.py frame_rate time path
    #    0           1      2     3
    mc = Record(int(sys.argv[1]),int(sys.argv[2]),sys.argv[3])
    mc.startMain()
