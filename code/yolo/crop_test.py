import colorsys
import os
from pathlib import Path
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import cv2
import re


import numpy as np


voltages = ["snr","snr-low","snr-mid"]

#voltages = ['']
for volt in voltages: 
    MAIN_PATH = f"../data/policies-indoor-cafe/{volt}/"# f"../data/policies/random/{volt}/"
    WRITE_TEXT = True
    WRITE_E = True 
    WRITE_GT = True
    TEXT_PATH_GT = "input/ground-truth/"
    TEXT_PATH_E  = "input/detection-results/"
    SAVE_IMG = False
    IMG_PATH = "input/images-optional/"
    COMBINE_IMG = True
    IMG_PATH_COMB ="input/images-combine-person/" #"input/images-combine/" 
    LABELS = ['person']
    CROP_E = (325,940)
    CROP_GT = (235,800)

    files = os.listdir(MAIN_PATH)
    files_base = list(filter(lambda x : (x[0]=='g') and (x[-1]=='g'),files))
    files_base = list(map(lambda x: x[2:],files_base))
    files_order = list(map(lambda x:int(re.findall(r'\d+', x)[0]), files_base))
    file_i = list(zip(files_order,files_base))
    files_base = list(zip(*sorted(file_i)))[1] 
    #print(files_base)
    for idx in range(0,len(files_base)): 
        file_img = files_base[idx]
        file_gt = "gt"+file_img
        file_e  = "e" +file_img 
        #load images 
        print(MAIN_PATH+file_gt)

        image_gt = cv2.imread(MAIN_PATH+file_gt, cv2.IMREAD_COLOR)
        image_e  = cv2.imread(MAIN_PATH+file_e , cv2.IMREAD_COLOR)
        #crop images 
        image_gt = image_gt[118:738,150:850]
        image_e = image_e[30:650,240:950]

        comb_img = np.concatenate((image_gt,image_e),axis=1)
        print(comb_img)
            #save combined image 
        #cv2.imwrite(MAIN_PATH+IMG_PATH_COMB+file_img[:-4]+'.jpg',r_image_e)
        cv2.imshow('image',comb_img)
        #print("SHOW")
        cv2.waitKey(0)