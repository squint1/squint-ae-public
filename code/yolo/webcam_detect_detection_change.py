import colorsys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
import cv2
import time
import queue
import threading
import multiprocessing as mp
import vid_streamv3 as vs
import sys
from pathlib import Path
import requests
from webcam_detect import YOLO
from voltage_control import V_Ctrl

v = V_Ctrl()
q = queue.Queue()
#IOU range, Light range, period 
POLICY = [(range(0,50),range(0,255),20),(range(50,101),range(85,255),100),(range(50,101),range(0,85),20)]
PERIOD = 60 * 6
RECORD = True

set_voltage_endpoint = "http://10.153.94.217:5050/set?voltage="
import numpy as np
import tensorflow.compat.v1.keras.backend as K
import tensorflow as tf
tf.compat.v1.disable_eager_execution()
#from keras import backend as K
from keras.models import load_model
from keras.layers import Input

from yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3.utils import image_preporcess


LABELS = []


def Receive():
    print('start receive')
    cap = cv2.VideoCapture('rtsp://10.208.23.206:8554/unicast', cv2.CAP_FFMPEG)
    ret, frame = cap.read()
    q.put(frame)
    while ret:
        ret, frame = cap.read()
        q.put(frame)
    cap.release()

def Process():
    yolo = YOLO()
    print("start processing")

    # set start time to current time
    start_time = time.time()
    # displays the frame rate every 2 second
    display_time = 2
    # Set primarry FPS to 0
    fps = 0

    while True:
        if q.empty() != True:
            frame = q.get()
            # resize our captured frame if we need
            frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

            # detect object on our frame
            #r_image, ObjectsList = yolo.detect_img(frame)

            # show us frame with detection
            cv2.imshow("Web cam input", r_image)
            if cv2.waitKey(25) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break

            # calculate FPS
            fps += 1
            TIME = time.time() - start_time
            if TIME > display_time:
                print("FPS:", fps / TIME)
                fps = 0
                start_time = time.time()

    cv2.destroyAllWindows()
    yolo.close_session()


def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return 0
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
    boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou

'''
Main class
Source: https://github.com/sahilparekh/GStreamer-Python/blob/master/main_prg.py
'''
class mainStreamClass:
    def __init__(self):

        #Current Cam
        self.camProcess = None
        self.cam_queue = None
        self.stopbit = None
        self.camlink = 'rtsp://10.208.17.232:8554/unicast' #Add your RTSP cam link
        self.framerate = 30

    def startMain(self):

        #Initialize YOLO
        yolo = YOLO()

        #set  queue size
        self.cam_queue = mp.Queue(maxsize=5)

        #get all cams
        time.sleep(3)

        self.stopbit = mp.Event()
        self.camProcess = vs.StreamCapture(self.camlink,
                             self.stopbit,
                             self.cam_queue,
                            self.framerate)
        self.camProcess.start()

        # calculate FPS
        lastFTime = time.time()

        # frame counter
        cnt = 0

        try:
            PAST_INVTERVAL = 10
            past_detection = []
            IoU_V = 0
            period = 100
            HIGH_PERIOD = 8
            ambient_light = 255
            start_time = int(time.time())
            if(RECORD):
                os.system(f"python3 record.py 24 {PERIOD} ../data/policies-indoor/change/ &")
                v.measure(PERIOD,"./data/change-indoor")
            while time.time() - start_time <= PERIOD:

                if not self.cam_queue.empty():
                    # print('Got frame')
                    cmd, frame = self.cam_queue.get()


                    #calculate FPS
                    diffTime = time.time() - lastFTime
                    fps = 1 / diffTime
                    # print(fps)

                    lastFTime = time.time()

                    # if cmd == vs.StreamCommands.RESOLUTION:
                    #     pass #print(val)

                    

                    if cmd == vs.StreamCommands.FRAME:
                        if frame is not None:
                            cnt = cnt + 1

                            t = time.time() - start_time
                            if(PERIOD == 3*60):
                                if(t < 60):
                                    v.set_exposure(600)
                                if(t < 120 and t > 60):
                                    v.set_exposure(10000)
                                if(t> 120):
                                    v.set_exposure(10000)
                            else:
                                if(t < 120):
                                    v.set_exposure(500)
                                if(t < 240 and t > 120):
                                    v.set_exposure(750)
                                if(t> 240):
                                    v.set_exposure(10000)
                            


                            # resize our captured frame if we need
                            frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

                            # detect object on our frame
                            r_image, ObjectsList = yolo.detect_img(frame,['person'])
                            #calculate change metric 
                            total_diff = 0
                            diff = 0
                            #r_image, ObjectsList = yolo.detect_img(frame,['person'])
                            #print(ObjectsList,past_detection)
                            l_1 = []
                            l_2 = []
                            if(cnt % PAST_INVTERVAL ==0): 
                                l_1 = []
                                l_2 = []
                                IoU_T = 0
                                for obj_c in ObjectsList:
                                    for obj_p in past_detection:
                                        boxA = [obj_c[0],obj_c[1],obj_c[2],obj_c[3]]
                                        boxB = [obj_p[0],obj_p[1],obj_p[2],obj_p[3]]
                                        IoU = bb_intersection_over_union(boxA,boxB)
                                        IoU_T = IoU + IoU_T
                                        #print(obj_p)
                                        #diff = (obj_c[4] - obj_p[4])**2 + (obj_c[5] - obj_p[5])**2 
                                        #if(diff<diff_min):
                                            #diff_min = diff
                                    #total_diff = total_diff + diff_min
                                if(len(ObjectsList) != 0):
                                    IoU_T = IoU_T / len(ObjectsList)
                                if(len(ObjectsList) != len(past_detection)):
                                    IoU_T = 0 
                                elif(len(ObjectsList) ==0 and len(past_detection) ==0 ):
                                    IoU_T = 100
                                 
                                past_detection = ObjectsList
                                #delta_dist = int(abs(past_dist - total_diff))
                                IoU_V = int(IoU_T  * 100)
                                #print(IoU_V)
                                #print(IoU_V)
                                #past_dist = total_diff

                            if cnt%period < HIGH_PERIOD:
                                if(cnt%HIGH_PERIOD == HIGH_PERIOD-1):
                                    ambient_light = int(np.mean(frame))
                                    #print(ambient_light)
                                v.set_voltage(2.8)
                                #print("HIGH")
                            else:
                                v.set_voltage(1.4)

                            for rangeC,rangeL,p in POLICY:
                                # print(ambient_light,rangeL)
                                if (IoU_V in rangeC) and ambient_light in rangeL :
                                    #print("WOOOT")
                                    period = p
                                    print((IoU_V, ambient_light,period))
                                    break 
                            #print(period)

                                #print("LOW")
                            #print(ObjectsList)
                            cv2.imshow('Cam: ' + self.camlink, r_image)
                            cv2.waitKey(1)


            if(RECORD):
                v.write_times("../data/policies-indoor/change/times.json")




                            # show us frame with detection
                            

        except KeyboardInterrupt:
            print('Caught Keyboard interrupt')

        except:
            e = sys.exc_info()
            print('Caught Main Exception')
            print(e)

        self.stopCamStream()
        cv2.destroyAllWindows()
        yolo.close_session()


    def stopCamStream(self):
        print('in stopCamStream')

        if self.stopbit is not None:
            self.stopbit.set()
            while not self.cam_queue.empty():
                try:
                    _ = self.cam_queue.get()
                except:
                    break
                self.cam_queue.close()

            self.camProcess.join()



if __name__=="__main__":
    # yolo = YOLO()

    # p1 = threading.Thread(target = Receive)
    # p2 = threading.Thread(target = Process)
    # p1.start()
    # p2.start()

    # set start time to current time
    # start_time = time.time()
    # # displays the frame rate every 2 second
    # display_time = 2
    # # Set primarry FPS to 0
    # fps = 0

    # we create the video capture object cap
    # cap = cv2.VideoCapture(0)
    # if not cap.isOpened():
    #    raise IOError("We cannot open webcam")

    # RTSP_URL = 'rtsp://10.208.17.232:8554/unicast'

    # cap = cv2.VideoCapture(RTSP_URL, cv2.CAP_FFMPEG)
    # if not cap.isOpened():
    #    print('Cannot open RTSP stream')
    #    exit(-1)

    #Labels 
    LABELS = ["person"]

    mc = mainStreamClass()
    mc.startMain()

    # while True:
    #     ret, frame = cap.read()
    #     if not(ret):
    #         print('No frame')
    #         exit(-1)
    #     # resize our captured frame if we need
    #     frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

    #     # detect object on our frame
    #     r_image, ObjectsList = yolo.detect_img(frame)

    #     # show us frame with detection
    #     cv2.imshow("Web cam input", r_image)
    #     if cv2.waitKey(25) & 0xFF == ord("q"):
    #         cv2.destroyAllWindows()
    #         break

    #     # calculate FPS
    #     fps += 1
    #     TIME = time.time() - start_time
    #     if TIME > display_time:
    #         print("FPS:", fps / TIME)
    #         fps = 0
    #         start_time = time.time()


    # cap.release()
    # cv2.destroyAllWindows()
    # yolo.close_session()
