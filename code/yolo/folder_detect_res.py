import colorsys
import os
from pathlib import Path
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
import cv2


import numpy as np
from keras import backend as K
from keras.models import load_model
from keras.layers import Input

from yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3.utils import image_preporcess

from image_detect import YOLO

#initialize YOLO 
yolo = YOLO()
voltages = ["480p-cafe","480p-indoor"]

#voltages = ['']
for volt in voltages: 
    MAIN_PATH = f"../data/{volt}/"# f"../data/policies/random/{volt}/"
    WRITE_TEXT = True
    WRITE_E = False
    WRITE_GT = True
    TEXT_PATH_GT = "input/ground-truth/"#"input/ground-truth/"
    TEXT_PATH_E  = "input/detection-results/"
    SAVE_IMG = False
    IMG_PATH = "input/images-optional/"
    COMBINE_IMG = False
    IMG_PATH_COMB = "input/images-combine/" # "input/images-combine-person/"
    LABELS = None#['person']
    CROP_E = (190,940)
    CROP_GT = (147,940)

    #create paths if non-existant 
    Path(MAIN_PATH+TEXT_PATH_GT).mkdir(parents=True, exist_ok=True)
    Path(MAIN_PATH+TEXT_PATH_E).mkdir(parents=True, exist_ok=True)
    Path(MAIN_PATH+IMG_PATH).mkdir(parents=True, exist_ok=True)
    Path(MAIN_PATH+IMG_PATH_COMB).mkdir(parents=True, exist_ok=True)



    files = os.listdir(MAIN_PATH)
    files_base = list(filter(lambda x : (x[0]=='g') and (x[-1]=='g'),files))
    files_base = list(map(lambda x: x[2:],files_base))

    try:
        for idx,file_img in enumerate(files_base): 
            file_gt = "gt"+file_img
            #file_e  = "e" +file_img 
            #load images 
            #print(MAIN_PATH+file_gt)

            image_gt = cv2.imread(MAIN_PATH+file_gt, cv2.IMREAD_COLOR)
            #image_e  = cv2.imread(MAIN_PATH+file_e , cv2.IMREAD_COLOR)
            #crop images 
            image_gt = image_gt[:,CROP_GT[0]:CROP_GT[1],:]
            image_e = image_e[:,CROP_E[0]:CROP_E[1],:]

            #save image 
            if(SAVE_IMG):
                cv2.imwrite(MAIN_PATH+IMG_PATH+file_img[:-4]+'.jpg',image_gt)

            #classify images
            r_image_gt, ObjectsList_gt = yolo.detect_img(image_gt,LABELS)
            r_image_e, ObjectsList_e = yolo.detect_img(image_e,LABELS) #UNDUE!!!

            if(COMBINE_IMG):
                #combine images
                comb_img = np.concatenate((r_image_gt,r_image_e),axis=1)
                #save combined image 
                cv2.imwrite(MAIN_PATH+IMG_PATH_COMB+file_img[:-4]+'.jpg',comb_img)


            if(WRITE_TEXT):
                #write gt 
                if(WRITE_GT):
                    with open(MAIN_PATH+TEXT_PATH_GT+file_img[:-4]+'.txt','w') as f:
                        for objectC in ObjectsList_gt: 
                            object_name = objectC[6].replace(" ","")
                            entry = f"{object_name} {objectC[7]} {int(objectC[1]*1.6)} {int(objectC[0]*1.6)} {int(objectC[3]*1.6)} {int(objectC[2]*1.6)} \n"
                            #print(objectC)
                            #print(entry)
                            f.write(entry)
                if(WRITE_E):
                    #write e
                    with open(MAIN_PATH+TEXT_PATH_E+file_img[:-4]+'.txt','w') as f:
                        for objectC in ObjectsList_e:
                            object_name = objectC[6].replace(" ","")
                            entry = f"{object_name} {objectC[7]} {objectC[1]} {objectC[0]} {objectC[3]} {objectC[2]} \n"
                            f.write(entry)

            if(idx % 100 == 0):
                print(idx/len(files_base))
    except:
        print(file_img)





