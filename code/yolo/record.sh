#!/bin/bash
# Define a timestamp function
timestamp() {
  x=$(echo '('$(date +"%s.%N") ' * 1000000)/1' | bc) # current time
}


# DVS camera capture
timestamp
timeout $2 gst-launch-1.0 rtspsrc latency=0 location="rtsp://10.208.23.206:8554/unicast" protocols="tcp" retry=50 timeout=50 tcp-timeout=5000000 drop-on-latency="true" ! rtph264depay ! avdec_h264 max-threads=2 output-corrupt="false" ! videoconvert ! videorate max-rate=30 drop-only="true" ! video/x-raw, format={BGR, GRAY8}, framerate='(fraction)'25/1 ! jpegenc ! multifilesink location="$1gt_N.A_$x.jpg" > /dev/null 2>&1 &
# Ground truth camera capture
timestamp
timeout $2 gst-launch-1.0 rtspsrc latency=0 location="rtsp://10.208.17.232:8554/unicast" protocols="tcp" retry=50 timeout=50 tcp-timeout=5000000 drop-on-latency="true" ! rtph264depay ! avdec_h264 max-threads=2 output-corrupt="false" ! videoconvert ! videorate max-rate=30 drop-only="true" ! video/x-raw, format={BGR, GRAY8}, framerate='(fraction)'25/1 ! jpegenc ! multifilesink location="$1e_N.A_$x.jpg" > /dev/null 2>&1 &
