import colorsys
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '0'
import cv2
import time
import queue
import threading
import multiprocessing as mp
import vid_streamv3 as vs
import sys
from pathlib import Path
import requests
from voltage_control import V_Ctrl
import random
from webcam_detect import YOLO

v = V_Ctrl()
RAND_FLOOR = 1.35
RAND_MAX = 2.84
PERIOD = 60*6
RECORD = False


q = queue.Queue()

set_voltage_endpoint = "http://10.153.94.217:5050/set?voltage="
import numpy as np
import tensorflow.compat.v1.keras.backend as K
import tensorflow as tf
tf.compat.v1.disable_eager_execution()
#from keras import backend as K
from keras.models import load_model
from keras.layers import Input

from yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3.utils import image_preporcess

LABELS = []




def Receive():
    print('start receive')
    cap = cv2.VideoCapture('rtsp://10.208.17.232:8554/unicast', cv2.CAP_FFMPEG)
    ret, frame = cap.read()
    q.put(frame)
    while ret:
        ret, frame = cap.read()
        q.put(frame)
    cap.release()

def Process():
    yolo = YOLO()
    print("start processing")

    # set start time to current time
    start_time = time.time()
    # displays the frame rate every 2 second
    display_time = 2
    # Set primarry FPS to 0
    fps = 0

    while True:
        if q.empty() != True:
            frame = q.get()
            # resize our captured frame if we need
            frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

            # detect object on our frame
            r_image, ObjectsList = yolo.detect_img(frame)

            # show us frame with detection
            cv2.imshow("Web cam input", r_image)
            if cv2.waitKey(25) & 0xFF == ord("q"):
                cv2.destroyAllWindows()
                break

            # calculate FPS
            fps += 1
            TIME = time.time() - start_time
            if TIME > display_time:
                #print("FPS:", fps / TIME)
                fps = 0
                start_time = time.time()

    cv2.destroyAllWindows()
    yolo.close_session()


'''
Main class
Source: https://github.com/sahilparekh/GStreamer-Python/blob/master/main_prg.py
'''
class mainStreamClass:
    def __init__(self):

        #Current Cam
        self.camProcess = None
        self.cam_queue = None
        self.stopbit = None
        self.camlink = 'rtsp://10.208.17.232:8554/unicast' #Add your RTSP cam link
        self.framerate = 10

    def startMain(self):

        #Initialize YOLO
        #yolo = YOLO()

        #set  queue size
        self.cam_queue = mp.Queue(maxsize=5)

        #get all cams
        time.sleep(3)

        self.stopbit = mp.Event()
        self.camProcess = vs.StreamCapture(self.camlink,
                             self.stopbit,
                             self.cam_queue,
                            self.framerate)
        self.camProcess.start()

        # calculate FPS
        lastFTime = time.time()

        # frame counter
        cnt = 0
        start_time = int(time.time())
        if(RECORD):
            os.system(f"python3 record.py 24 {PERIOD} ../data/random-room/ &")
            #v.measure(PERIOD,"./data/random-indoor-cafe")
        try:
            while time.time() - start_time <= PERIOD:

                if not self.cam_queue.empty():
                    # print('Got frame')
                    cmd, frame = self.cam_queue.get()


                    #calculate FPS
                    diffTime = time.time() - lastFTime
                    fps = 1 / diffTime
                    

                    lastFTime = time.time()

                    # if cmd == vs.StreamCommands.RESOLUTION:
                    #     pass #print(val)

                    if cmd == vs.StreamCommands.FRAME:
                        if frame is not None:
                            #set exposure 
                            t = time.time() - start_time
                            if(PERIOD == 3*60):
                                if(t < 60):
                                    v.set_exposure(500)
                                if(t < 120 and t > 60):
                                    v.set_exposure(750)
                                if(t> 120):
                                    v.set_exposure(10000)
                            else:
                                if(t < 120):
                                    v.set_exposure(500)
                                if(t < 240 and t > 120):
                                    v.set_exposure(750)
                                if(t> 240):
                                    v.set_exposure(10000)
                            # Random policy: Reconfigure voltage every other frame
                            #volt = random.uniform(RAND_FLOOR,RAND_MAX)
                            volt = random.choice([2.8,1.4])#[2.8,2.7,2.6,2.5,2.4,2.3,2.2,2.1,2.0,1.9,1.8,1.7,1.6,1.5,1.4])
                            #print(fps,volt)
                            #print()
                            v.set_voltage(volt)
                            # while(not set_succ):
                            #     #set voltage via end point
                            #     set_voltage_get = requests.get(set_voltage_endpoint + str(voltage))
                            #     print(set_voltage_get.json())
                            #     if(set_voltage_get.json()["success"] == True):
                            #         set_succ = True
                            #         print("GET SUCESS")
                            #     else:
                            #         time.sleep(15)

                            # Increment frame counter
                            cnt = cnt + 1


                            # resize our captured frame if we need
                            frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)
                            # detect object on our frame
                            #r_image, ObjectsList = yolo.detect_img(frame)
                            #print(ObjectsList)

                            # show us frame with detection
                            cv2.imshow('Cam: ' + self.camlink, frame)
                            cv2.waitKey(1)
            if(RECORD):
                v.write_times("../data/random-room/times.json")

        except KeyboardInterrupt:
            print('Caught Keyboard interrupt')
            #v.write_times('./random.json')

        except:
            e = sys.exc_info()
            print('Caught Main Exception')
            print(e)

        self.stopCamStream()
        cv2.destroyAllWindows()
        #yolo.close_session()


    def stopCamStream(self):
        print('in stopCamStream')

        if self.stopbit is not None:
            self.stopbit.set()
            while not self.cam_queue.empty():
                try:
                    _ = self.cam_queue.get()
                except:
                    break
                self.cam_queue.close()

            self.camProcess.join()



if __name__=="__main__":
    # yolo = YOLO()

    # p1 = threading.Thread(target = Receive)
    # p2 = threading.Thread(target = Process)
    # p1.start()
    # p2.start()

    # set start time to current time
    # start_time = time.time()
    # # displays the frame rate every 2 second
    # display_time = 2
    # # Set primarry FPS to 0
    # fps = 0

    # we create the video capture object cap
    # cap = cv2.VideoCapture(0)
    # if not cap.isOpened():
    #    raise IOError("We cannot open webcam")

    # RTSP_URL = 'rtsp://10.208.17.232:8554/unicast'

    # cap = cv2.VideoCapture(RTSP_URL, cv2.CAP_FFMPEG)
    # if not cap.isOpened():
    #    print('Cannot open RTSP stream')
    #    exit(-1)

    #Labels 
    LABELS = ["person"]

    mc = mainStreamClass()
    mc.startMain()
    mc.stopCamStream()
    #v.write_times("../data/policies/random/times.json")
    

    # while True:
    #     ret, frame = cap.read()
    #     if not(ret):
    #         print('No frame')
    #         exit(-1)
    #     # resize our captured frame if we need
    #     frame = cv2.resize(frame, None, fx=1.0, fy=1.0, interpolation=cv2.INTER_AREA)

    #     # detect object on our frame
    #     r_image, ObjectsList = yolo.detect_img(frame)

    #     # show us frame with detection
    #     cv2.imshow("Web cam input", r_image)
    #     if cv2.waitKey(25) & 0xFF == ord("q"):
    #         cv2.destroyAllWindows()
    #         break

    #     # calculate FPS
    #     fps += 1
    #     TIME = time.time() - start_time
    #     if TIME > display_time:
    #         print("FPS:", fps / TIME)
    #         fps = 0
    #         start_time = time.time()


    # cap.release()
    # cv2.destroyAllWindows()
    # yolo.close_session()
