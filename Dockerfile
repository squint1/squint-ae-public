# Use an official Python runtime as a base image
FROM python:3.8

# Set the working directory inside the container
WORKDIR /squint

# Install apt packages
RUN apt-get update -y && \
    apt-get install -y libgl1-mesa-glx



# Copy the contents of the GitLab repo folder to the working directory
COPY . /squint
#COPY camera_pose_estimation /squint
#COPY data_analysis /squint
#COPY mAP /squint
#COPY yolo /squint
#COPY req.txt /squint


# Install any required dependencies for your data processing scripts (if needed)
RUN pip install -r /squint/req.txt

# Expose the port if needed
EXPOSE 8888

# Run the IPython notebook server when the container starts
#CMD ["jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--no-browser", "--allow-root"]

# Start an interactive bash shell when the container starts
CMD ["/bin/bash"]
